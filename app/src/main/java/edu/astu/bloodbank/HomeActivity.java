package edu.astu.bloodbank;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

import edu.astu.bloodbank.Room.Database.SqliteHelper;
import edu.astu.bloodbank.Room.Entities.Myself;

public class HomeActivity extends AppCompatActivity {

    private Myself mMe;
    private EditText mFullName;
    private EditText mEmail;
    private ToggleButton mSex;
    private Button mLogoutButton;
    private Button mSendButton;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_edit_account);
        initializeMyself();
        mFullName = (EditText) findViewById(R.id.name);
        mEmail = (EditText) findViewById(R.id.email);
        mSex = (ToggleButton) findViewById(R.id.sex);

        mFullName.setText(mMe.getName());
        mEmail.setText(mMe.getEmail());
        switch (mMe.getSex()){
            case "Male":
                mSex.setChecked(false);
                break;
            case "Female":
                mSex.setChecked(true);
                break;
            default:
                Toast.makeText(this, mMe.getSex(), Toast.LENGTH_SHORT).show();
        }

        mLogoutButton = (Button) findViewById(R.id.logoutButton);
        mLogoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logoutAction();
                goToLogin();
            }
        });
    }

    private void goToLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    private void logoutAction() {
        SqliteHelper helper = new SqliteHelper(this);
        SQLiteDatabase database= helper.getWritableDatabase();
        database.delete(SqliteHelper.TABLE_NAME,null,null );
        database.close();
    }

    private void initializeMyself() {
        SqliteHelper helper = new SqliteHelper(this);
        SQLiteDatabase database= helper.getWritableDatabase();
        ArrayList<Myself> users= new ArrayList();
        Cursor listCursor = database.query(SqliteHelper.TABLE_NAME,
                new String [] {SqliteHelper.ID, SqliteHelper.NAME,SqliteHelper.SEX,SqliteHelper.EMAIL},
                null, null, null, null, SqliteHelper.NAME);
        while (listCursor.moveToNext()) {
            Long id = listCursor.getLong(0);
            String name= listCursor.getString(1);
            String sex= listCursor.getString(2);
            String email= listCursor.getString(3);
            Myself t = new Myself(id,name,sex,email);
            users.add(t);
        }
        mMe = users.get(0);
    }
}
