package edu.astu.bloodbank;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import edu.astu.bloodbank.Room.Database.SqliteHelper;

public class LoginActivity extends AppCompatActivity {
    private Button mLoginButton;
    private EditText mEmailInput;
    private EditText mPasswordInput;
    private TextView mSignupLink;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_login_activity);

        mLoginButton = (Button) findViewById(R.id.logInButton);
        mEmailInput = (EditText) findViewById(R.id.email);
        mPasswordInput = (EditText) findViewById(R.id.password);
        mSignupLink = (TextView) findViewById(R.id.linkToSignup);

        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginAction();
            }
        });
    }

    private void loginAction() {
        String email = String.valueOf(mEmailInput.getText());
        String password = String.valueOf(mPasswordInput.getText());
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = WebApi.LOGIN_URL;
        JSONObject user_params = new JSONObject();
        try {
            String key_email = "email";
            String key_password = "password";
            user_params.put(key_email, email);
            user_params.put(key_password, password);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, user_params, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.get("status").toString().equals("success")){
                                JSONObject  userdata = (JSONObject ) response.get("data");
                                Long id = Long.valueOf(userdata.get("id").toString());
                                String email = userdata.get("email").toString();
                                String name = userdata.get("name").toString();
                                String sex = userdata.get("sex").toString();
                                Long logged_in_user_id =saveUser(id,name,email,sex);
                                goToMain();
//                                Toast.makeText(LoginActivity.this, String.format("%s%s", response.get("data").toString(), logged_in_user_id), Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            Toast.makeText(LoginActivity.this, response.get("status").toString(), Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(LoginActivity.this, "You are not connected to Internet. Please double check.", Toast.LENGTH_LONG).show();
                    }
                }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String,String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return super.getParams();
            }
        };

        jsObjRequest.setRetryPolicy(
                new RetryPolicy() {
                    @Override
                    public int getCurrentTimeout() {
                        return 50000;
                    }
                    @Override
                    public int getCurrentRetryCount() {
                        return 50000;
                    }
                    @Override
                    public void retry(VolleyError error) throws VolleyError {
                        Toast.makeText(LoginActivity.this, "Error2 : "+error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
        queue.add(jsObjRequest);
    }

    private Long saveUser(Long id,String name, String email, String sex) {
        SqliteHelper helper = new SqliteHelper(this);
        SQLiteDatabase database= helper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(SqliteHelper.NAME, name);
        values.put(SqliteHelper.EMAIL, email);
        values.put(SqliteHelper.ID, id);
        values.put(SqliteHelper.SEX, sex);
        return (database.insert(SqliteHelper.TABLE_NAME, null, values));
    }

    public void goToMain(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void goToSignUp(View view){
        Intent intent = new Intent(this, SignupActivity.class);
        startActivity(intent);
    }
}
