package edu.astu.bloodbank;

public class WebApi {

    public static final String BASE_URL = "http://10.42.0.1:1234/";
//    public static final String BASE_URL = "http://10.240.68.76:1234/";
    public static final String LOGIN_URL = BASE_URL + "auth/login.php";
    public static final String SIGNUP_URL = BASE_URL + "auth/signup.php";
}
