package edu.astu.bloodbank;


import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import java.util.ArrayList;

import edu.astu.bloodbank.Main.ConnectFragment;
import edu.astu.bloodbank.Main.DataModel;
import edu.astu.bloodbank.Main.DrawerItemCustomAdapter;
import edu.astu.bloodbank.Room.Database.SqliteHelper;
import edu.astu.bloodbank.Room.Entities.Myself;
import edu.astu.bloodbank.Room.Entities.User;

public class MainActivity extends AppCompatActivity {

    private Myself mMe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        SqliteHelper helper = new SqliteHelper(this);
//        SQLiteDatabase database= helper.getWritableDatabase();
        try {
            initializeMyself();
            goToHome();
        }catch (Exception e){
            logoutAction();
            goToLogin();
        }


    }

    private void goToHome() {
        Intent intent = new Intent(this, edu.astu.bloodbank.Main.MainActivity.class);
        startActivity(intent);
    }


    private void initializeMyself() {
        SqliteHelper helper = new SqliteHelper(this);
        SQLiteDatabase database= helper.getWritableDatabase();
        ArrayList<Myself> users= new ArrayList();
        Cursor listCursor = database.query(SqliteHelper.TABLE_NAME,
                new String [] {SqliteHelper.ID, SqliteHelper.NAME,SqliteHelper.SEX,SqliteHelper.EMAIL},
                null, null, null, null, SqliteHelper.NAME);
        while (listCursor.moveToNext()) {
            Long id = listCursor.getLong(0);
            String name= listCursor.getString(1);
            String sex= listCursor.getString(2);
            String email= listCursor.getString(3);
            Myself t = new Myself(id,name,sex,email);
            users.add(t);
        }
        mMe = users.get(0);
    }


    private void goToLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    private void logoutAction() {
        SqliteHelper helper = new SqliteHelper(this);
        SQLiteDatabase database= helper.getWritableDatabase();
        database.delete(SqliteHelper.TABLE_NAME,null,null );
        database.close();
    }
}
