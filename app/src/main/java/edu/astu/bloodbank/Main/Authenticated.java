package edu.astu.bloodbank.Main;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import edu.astu.bloodbank.Room.Database.SqliteHelper;
import edu.astu.bloodbank.Room.Entities.Myself;

public class Authenticated {


    public static Myself getMe(Context context) {
        SqliteHelper helper = new SqliteHelper(context);
        SQLiteDatabase database= helper.getWritableDatabase();
        ArrayList<Myself> users= new ArrayList();
        Cursor listCursor = database.query(SqliteHelper.TABLE_NAME,
                new String [] {SqliteHelper.ID, SqliteHelper.NAME,SqliteHelper.SEX,SqliteHelper.EMAIL},
                null, null, null, null, SqliteHelper.NAME);
        while (listCursor.moveToNext()) {
            Long id = listCursor.getLong(0);
            String name= listCursor.getString(1);
            String sex= listCursor.getString(2);
            String email= listCursor.getString(3);
            Myself t = new Myself(id,name,sex,email);
            users.add(t);
        }
        return users.get(0);
    }
}
