package edu.astu.bloodbank.Main;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.ArrayList;

import edu.astu.bloodbank.LoginActivity;
import edu.astu.bloodbank.R;
import edu.astu.bloodbank.Room.Database.SqliteHelper;
import edu.astu.bloodbank.Room.Entities.Myself;

class EditAccountFragment extends androidx.fragment.app.Fragment {
    private Myself mMe;
    private EditText mFullName;
    private EditText mEmail;
    private ToggleButton mSex;
    private Button mLogoutButton;
    private Button mSendButton;
    private Context mContext;

    public EditAccountFragment(Context context){
        mContext = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_edit_account, container, false);

        initializeMyself();
        mFullName = (EditText) rootView.findViewById(R.id.name);
        mEmail = (EditText) rootView.findViewById(R.id.email);
        mSex = (ToggleButton) rootView.findViewById(R.id.sex);
        mFullName.setText(mMe.getName());
        mEmail.setText(mMe.getEmail());
        switch (mMe.getSex()){
            case "Male":
                mSex.setChecked(false);
                break;
            case "Female":
                mSex.setChecked(true);
                break;
            default:
                Toast.makeText(mContext, mMe.getSex(), Toast.LENGTH_SHORT).show();
        }

        mLogoutButton = (Button) rootView.findViewById(R.id.logoutButton);
        mLogoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logoutAction();
                goToLogin();
            }
        });
        return rootView;
    }


    private void goToLogin() {
        Intent intent = new Intent(mContext, LoginActivity.class);
        startActivity(intent);
    }

    private void logoutAction() {
        SqliteHelper helper = new SqliteHelper(mContext);
        SQLiteDatabase database= helper.getWritableDatabase();
        database.delete(SqliteHelper.TABLE_NAME,null,null );
        database.close();
    }

    private void initializeMyself() {
        SqliteHelper helper = new SqliteHelper(mContext);
        SQLiteDatabase database= helper.getWritableDatabase();
        ArrayList<Myself> users= new ArrayList();
        Cursor listCursor = database.query(SqliteHelper.TABLE_NAME,
                new String [] {SqliteHelper.ID, SqliteHelper.NAME,SqliteHelper.SEX,SqliteHelper.EMAIL},
                null, null, null, null, SqliteHelper.NAME);
        while (listCursor.moveToNext()) {
            Long id = listCursor.getLong(0);
            String name= listCursor.getString(1);
            String sex= listCursor.getString(2);
            String email= listCursor.getString(3);
            Myself t = new Myself(id,name,sex,email);
            users.add(t);
        }
        mMe = users.get(0);
    }
}
