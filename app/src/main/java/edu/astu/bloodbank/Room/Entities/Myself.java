package edu.astu.bloodbank.Room.Entities;

public class Myself {

    private Long mId;
    private String mName;
    private String mSex;
    private String mEmail;

    public Myself(Long id, String name, String sex, String email){
        mId = id;
        mName = name;
        mSex = sex;
        mEmail = email;
    }
    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getSex() {
        return mSex;
    }

    public void setSex(String sex) {
        mSex = sex;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }
}
