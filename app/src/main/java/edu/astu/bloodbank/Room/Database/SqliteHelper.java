package edu.astu.bloodbank.Room.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class SqliteHelper extends SQLiteOpenHelper {
    public static final String DBNAME = "bloodbank.sqlite";
    public static final int VERSION =1;
    public static final String TABLE_NAME = "users";
    public static final String ID= "id";
    public static final String NAME="name";
    public static final String SEX="sex";
    public static final String EMAIL="email";


    public SqliteHelper(@Nullable Context context) {
        super(context, DBNAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createDatabase(db);
    }

    private void createDatabase(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_NAME + "(" +
                ID + " integer primary key autoincrement not null, " +
                NAME + " text, " + SEX + " text," + EMAIL + " text"  + ");"
        );
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
