package edu.astu.bloodbank.Room.Database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import edu.astu.bloodbank.Room.Dao.UserDao;
import edu.astu.bloodbank.Room.Entities.User;

@Database(entities = {User.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract UserDao userDao();
}
