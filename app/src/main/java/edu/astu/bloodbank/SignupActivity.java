package edu.astu.bloodbank;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SignupActivity extends AppCompatActivity {
    private EditText mFullName;
    private EditText mEmail;
    private ToggleButton mSex;
    private EditText mPassword1;
    private EditText mPassword2;
    private Button mCancelButton;
    private Button mSendButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_signin_activity);
        mFullName = (EditText) findViewById(R.id.name);
        mEmail = (EditText) findViewById(R.id.email);
        mSex = (ToggleButton) findViewById(R.id.sex);
        mPassword1 = (EditText) findViewById(R.id.password1);
        mPassword2 = (EditText) findViewById(R.id.password2);
        mSendButton = (Button) findViewById(R.id.sendButton);
        mCancelButton = (Button) findViewById(R.id.cancelButton);
        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                send();
            }
        });
        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel();
            }
        });
    }

    private void cancel() {
        mFullName.setText("");
        mEmail.setText("");
        mPassword1.setText("");
        mPassword2.setText("");
        mSex.setTextOn("Female");
    }

    private void send() {
        final String fullname = String.valueOf(mFullName.getText());
        final String sex = String.valueOf( mSex.getText() );
        final String email = String.valueOf( mEmail.getText());
        final String password1 = String.valueOf( mPassword1.getText());
        final String password2 = String.valueOf( mPassword2.getText());

        if( !password1.equals(password2) ){
            mPassword1.setText("");
            mPassword2.setText("");
            Toast.makeText(this, "passwords do not match ", Toast.LENGTH_SHORT).show();
            return;
        }
        if(password1.isEmpty() ){
            mPassword1.setText("");
            mPassword2.setText("");
            Toast.makeText(this, "empty password provided ", Toast.LENGTH_SHORT).show();
            return;
        }
//        Toast.makeText(this, fullname + " "+ sex + " "+ password1, Toast.LENGTH_LONG).show();
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = WebApi.SIGNUP_URL;
        JSONObject user_params = new JSONObject();
        try {
            String key_email = "email";
            String key_password = "password";
            String key_name= "name";
            String key_sex = "sex";
            user_params.put(key_email, email);
            user_params.put(key_password, password2);
            user_params.put(key_name, fullname);
            user_params.put(key_sex, sex);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, user_params, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if(response.get("status").toString().equals("success")){
                                Toast.makeText(SignupActivity.this, "Succesfully registered! Login with your credentials.", Toast.LENGTH_SHORT).show();
                                goToMain();
                            }else{
                                Toast.makeText(SignupActivity.this, "Email address already taken.", Toast.LENGTH_LONG).show();
//                                Toast.makeText(SignupActivity.this, response.get("message").toString(), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(SignupActivity.this, "You are not connected to Internet. Please double check.", Toast.LENGTH_LONG).show();
                    }
                }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String,String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return super.getParams();
            }
        };
        jsObjRequest.setRetryPolicy(
                new RetryPolicy() {
                    @Override
                    public int getCurrentTimeout() {
                        return 50000;
                    }
                    @Override
                    public int getCurrentRetryCount() {
                        return 50000;
                    }
                    @Override
                    public void retry(VolleyError error) throws VolleyError {
                        Toast.makeText(SignupActivity.this, "Error2 : "+error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
        queue.add(jsObjRequest);
    }

    private void goToMain() {

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
